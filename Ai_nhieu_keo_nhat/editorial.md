# Ai nhiều kẹo nhất - Editorial

## Subtask 1:

Khởi tạo mảng rỗng gồm n phần tử với giá trị ban đầu là $0$. 
Với mỗi bộ số $a,b,k$, ta cộng $k$ vào từng phần tử trong đoạn [$a,b$] của mảng.

Sau khi thực hiện xong việc cập nhật giá trị cho mảng, ta duyệt toàn bộ mảng để tìm ra giá trị lớn nhất.

Độ phức tạp: $O(n \times m)$

## Subtask 2:

Ta có thể thực hiện việc cộng $k$ vào từng phần tử trong đoạn [$a,b$] trong $O(1)$ bằng cách cộng $k$ vào phần tử ở vị trí $a$ và $-k$ vào vị trí $(b+1)$

Khi thực hiện loại cập nhật này, phần tử thứ $i$ trong mảng sẽ là giá trị cần cập nhật để có được số kẹo của học sinh tại vị trí $i$, bởi vì ta đã cộng $k$ vào phần tử tại vị trí $a$ và $-k$ tại vị trí $(b+1)$

Từ đó, ta có thể thực hiện việc chia kẹo trong thời gian $O(m)$. Bây giờ ta phải tìm số lớn nhất trong mảng ban đầu, tức là thực hiện việc cộng giá trị tại $i-1$ vào $i$ để cập nhật số kẹo của mỗi bạn. Sau đó tìm giá trị lớn nhất trong mảng.

Độ phức tạp: $O(max(n,m))$