# Ai nhiều kẹo nhất

Hôm nay là ngày Noel, các cô bảo mẫu trường mẫu giáo của bé Đạt quyết định tặng quà cho các bé trong trường. Vào giờ ra chơi, các bạn sẽ xếp thành một hàng ngang, mỗi bảo mẫu sẽ tặng $x$ viên kẹo cho $y$ bạn liên tục trong hàng. Là một người thông minh và lanh trí, bé Đạt nhận ra rằng số kẹo nhận được của mỗi bạn sẽ không đều nhau. Cậu tò mò rằng ai sẽ là người nhận được nhiều kẹo nhất. Hãy giúp bé Đạt tìm ra người đó. 

## Input

- Dòng thứ nhất chứa $2$ số nguyên $n$ và $m$ $-$ số bạn học sinh và số bảo mẫu trong trường.
- $m$ dòng tiếp theo, mỗi dòng chứa $3$ số nguyên $a, b$ và $k$ $-$ vị trí bạn đầu tiên, bạn cuối cùng và số kẹo mà bảo mẫu sẽ tặng cho các bạn từ vị trí $a$ đến $b$.

## Constraints

- $3 \le n \le 10^7$
- $1 \le m \le 2 \times 10^5$
- $1 \le a \le b \le n$
- $0 \le k \le 10^9$

## Subtasks

- $60\%$ số điểm: 
    - $3 \le n \le 10^4$
    - $1 \le m \le 10^3$
- $40\%$ số điểm còn lại: không có ràng buộc gì thêm 
## Output

Một số nguyên duy nhất chỉ ra số kẹo lớn nhất của một bạn trong trường nhận được.

## Sample Input

```
5 3
1 2 100
2 5 100
3 4 100
```

## Sample Output

```
200
```

## Explanation

Sau khi bảo mẫu thứ nhất cho kẹo thì số kẹo của các bạn lần lượt là: $100\; 100\; 0\; 0\; 0$  

Sau khi bảo mẫu thứ hai cho kẹo thì số kẹo của các bạn lần lượt là: $100\; 200\; 100\; 100\; 100$ 

Sau khi bảo mẫu thứ ba cho kẹo thì số kẹo của các bạn lần lượt là: $100\; 200\; 100\; 200\; 200$ 

Vậy số kẹo lớn nhất mà một bạn nhận được là $200$

